﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Heroes
{
	public class Wizard : Hero, IHero
	{
		public string FavouriteSpell { get; set; }
		public Wizard(string name, int distance = 2) : base(name)
		{
			Hp = 30;
			Mana = 100;
			ArmorRating = 0;
			Distance = distance;
			MoveType = "walk";
		}

		public virtual string Attack()
		{
			string attackString = $"{Name} attacks with {FavouriteSpell} and does {AttackDamage} damage.";
			return attackString;
		}
	}
}