﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class AxMan : Warrior
    {
        public AxMan(string name, int distance) : base(name, distance)
        {
            WeaponType = "Battle Ax";
            ArmorRating = 70;
            AttackDamage = 100;
        }
    }
}
