﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Mage : Wizard
    {
        public Mage(string name, int distance) : base(name, distance)
        {
            FavouriteSpell = "Ball of Spectral Disruption";
            AttackDamage = 60;
        }
    }
}
