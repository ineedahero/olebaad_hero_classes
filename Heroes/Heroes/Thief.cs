﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Thief : Hero, IHero
    {
        public string WeaponType { get; set; }
        public Thief(string name) : base(name)
        {
            Hp = 60;
            Mana = 10;
            MoveType = "sneak";
        }

        public virtual string Attack()
        {
            string attackString = $"{Name} attacks with his {WeaponType} and does {AttackDamage.ToString()} damage.";
            return attackString;
        }
    }
}
