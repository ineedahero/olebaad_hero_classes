﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Warrior : Hero, IHero
    {
        public string WeaponType { get; set; }
        public Warrior(string name, int distance = 3) : base(name)
        {
            Hp = 60;
            Mana = 30;
            Distance = distance;
            MoveType = "run";
        }
        public string Attack()
        {
            string attackString = $"{Name} attacks with his {WeaponType} and does {AttackDamage.ToString()} damage.";
            return attackString;
        }
    }
}
