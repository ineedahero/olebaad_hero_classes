﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Robber : Thief
    {
        public Robber(string name) : base(name)
        {
            ArmorRating = 40;
            Distance = 2;
            AttackDamage = 10;
            WeaponType = "Short Sword";
        }
    }
}
