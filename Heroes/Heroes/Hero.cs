﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    public class Hero
    {
        public int Hp { get; set; }
        public int Mana { get; set; }
        public string Name { get; set; }
        public int ArmorRating { get; set; }
        public int Distance { get; set; }
        public string MoveType { get; set; }
        public int AttackDamage { get; set; }
        public Hero(string name)
        {
            Name = name;
        }

        public string Move()
        {
            string moveString = $"{Name} {MoveType}s {Distance} feet forward";
            return moveString;
        }
    }
}