﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Burglar : Thief
    {
        public Burglar(string name) : base(name)
        {
            ArmorRating = 20;
            Distance = 1;
            AttackDamage = 10;
            WeaponType = "Small Dagger";
        }
    }
}

