﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Knigth : Warrior
    {
        public Knigth(string name, int distance) : base(name, distance)
        {
            WeaponType = "Bastard Sword";
            ArmorRating = 100;
            AttackDamage = 70;
        }
    }
}
