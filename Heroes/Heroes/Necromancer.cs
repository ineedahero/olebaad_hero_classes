﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    class Necromancer : Wizard
    {
        public Necromancer(string name, int distance) : base(name, distance)
        {
            FavouriteSpell = "Resurrect";
        }
        public override string Attack()
        {
            string attackString = $"{Name} uses his {FavouriteSpell}-spell and brings his allies back to life.";
            return attackString;
        }
    }
}
