﻿using System;

namespace Heroes
{
	public interface IHero
	{
		string Attack();
	}

}